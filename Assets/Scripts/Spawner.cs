﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    public GameObject[] Bricks;
    public int height = 20;
    public int width = 10;
    public Transform[,] grid;
    private bool gameOver = false;
    int points = 134;
    public GameObject GameOverScreen;
    public GameObject GameStartScreen;
    public Text scoreText;
    public bool gameStarted = false;
    int fullSpaces = 0;

    // Start is called before the first frame update
    void Start()
    {
        grid = new Transform[width, height];
    }

    public void NewBrick()
    {
        if (!gameOver)
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, 16] != null)
                {
                    GameOver();
                    return;
                }
            }

            GameObject newBrick = Instantiate(Bricks[Random.Range(0, Bricks.Length)], transform.position, Quaternion.identity);
            newBrick.GetComponent<BrickFall>().spawnerScript = this;
        }
    }

    public void GameOver() // Ruutujen lukumäärä / 100% = yhden ruudun prosenttiarvo. Käytä tätä loppulaskennassa 150 - 16 = 134 
    {
        Debug.Log("GAME END");
        gameOver = true;

        for (int i = 0; i < width; i++)
        {
            for (int a = 0; a < 15; a++)
            {
                if (grid[i, a] == null) //Counting empty spaces
                {
                    points--;
                }

                if (grid[i, a] != null) //Counting full spaces
                {
                    fullSpaces++;
                }
            }
        }

        Debug.Log("POINTS:" + points);
        Debug.Log("Full spaces:" + fullSpaces);

        //yourFloat.ToString("F2")


        GameOverScreen.SetActive(true);

        float scorePer = points * 100.0f / 134.0f; 
        scoreText.text = "You fixed " + scorePer.ToString("F2") + "% of the house!";
    }

    void Update()
    {
        if (gameOver)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene(0);
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
                Debug.Log("Game quit");
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !gameStarted)
        {
            GameStartScreen.SetActive(false);
            NewBrick();
            gameStarted = true;
        }
    }
}
