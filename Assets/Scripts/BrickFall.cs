﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickFall : MonoBehaviour
{
    public Vector3 rotationPoint;
    private float previousTime = 0.0f;
    public float fallTime = 0.8f;
    public Spawner spawnerScript;

    public bool obstacle = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.LeftArrow) && !obstacle)
        if (Input.GetKeyDown(KeyCode.A) && !obstacle)
        {
            transform.position += new Vector3(-1, 0, 0);
            if (!ValidMove())
               transform.position -= new Vector3(-1, 0, 0);
        }
        //else if (Input.GetKeyDown(KeyCode.RightArrow) && !obstacle)
        else if (Input.GetKeyDown(KeyCode.D) && !obstacle)
        {
            transform.position += new Vector3(1, 0, 0);
            if (!ValidMove())
                transform.position -= new Vector3(1, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.J) && !obstacle)
        {
            transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), 90);
            if (!ValidMove())
                transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), -90);
        }
        else if (Input.GetKeyDown(KeyCode.K) && !obstacle)
        {
            transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), -90);
            if (!ValidMove())
                transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), 90);
        }
        else if (Input.GetKeyDown(KeyCode.W) && !obstacle) //Hard Drop
        {
            while (true) //This is not OK
            {
                transform.position += new Vector3(0, -1, 0);
                if (!ValidMove())
                {
                    transform.position -= new Vector3(0, -1, 0);
                    break;
                }
            }
        }

        //if (Time.time - previousTime > (Input.GetKey(KeyCode.DownArrow) ? fallTime / 10 : fallTime) && !obstacle)
        if (Time.time - previousTime > (Input.GetKey(KeyCode.S) ? fallTime / 10 : fallTime) && !obstacle)
        {
            transform.position += new Vector3(0, -1, 0);
            if (!ValidMove())
            {
                transform.position -= new Vector3(0, -1, 0);
                AddToGrid();
                this.enabled = false;
                spawnerScript.NewBrick();
            }
                
            previousTime = Time.time;
        }

        if (obstacle)
        {
            AddToGrid();
        }
    }

    

    void AddToGrid()
    {
        foreach(Transform children in transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);

            spawnerScript.grid[roundedX, roundedY] = children;
        }
    }

    bool ValidMove()
    {
        foreach(Transform children in transform)
        {
            int roundedX = Mathf.RoundToInt(children.transform.position.x);
            int roundedY = Mathf.RoundToInt(children.transform.position.y);

            if (roundedX < 0 || roundedX >= spawnerScript.width || roundedY < 0 || roundedY >= spawnerScript.height)
            {
                return false;
            }

            if (spawnerScript.grid[roundedX, roundedY] != null)
            {
                return false;
            }
        }

        return true;
    }
}
